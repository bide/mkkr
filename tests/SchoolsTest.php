<?php

include 'Schools.php';

class SchoolsTest extends \PHPUnit\Framework\TestCase
{

    private $schools;

    protected function SetUp() :void{
        $this->schools = new schools();
    }

    public function testStudents(){
        $schools = $this->schools->getSchoolByStudents( 4);

        $this->assertCount(10, $schools);

    }
}