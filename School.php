<?php


class School
{

    private $number;
    private $district;
    private $students;

    /**
     * Car constructor.
     * @param $number
     * @param $district
     * @param $students
     */
    public function __construct($number, $district, $students)
    {
        $this->number = $number;
        $this->district = $district;
        $this->students = $students;
    }


    /**
     * @return mixed
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * @param mixed $number
     */
    public function setNumber($number)
    {
        $this->number = $number;
    }

    /**
     * @return mixed
     */
    public function getDistrict()
    {
        return $this->district;
    }

    /**
     * @param mixed $district
     */
    public function setDistrict($district)
    {
        $this->district = $district;
    }

    /**
     * @return mixed
     */
    public function getStudents()
    {
        return $this->students;
    }

    /**
     * @param mixed $students
     */
    public function setStudents($students)
    {
        $this->students = $students;
    }
}