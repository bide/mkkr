<?php

include 'School.php';

class Schools
{
    private $shkoly;

    public function  __construct()
    {
        $this->shkoly = array(
            new School('13', 'Darnytskii', 5),
            new School('322', 'Dniprovskyi', 13),
            new School('343', 'Desnyanskyi', 11),
        ) ;
    }

    public function getSchoolByStudents($students){
        foreach ($this->shkoly as $school){
            if ($school->getStudents() == $students )
                return $school->getNumber();
        }

        return null;


    }

    public function getSchoolsCountByDistrict($district){
        $count = 0;

        foreach ($this->shkoly as $school){
            if($school->getDistrict() == $district){
                $count++;
            }
        }
        return $count;
    }
}